# QubesYubikey

Short script that can quickly add ones yubikey to your Vault VM.

Most of the code was taken from Micah Lee's blog post: [Blog Post](https://micahflee.com/2016/12/qubes-tip-making-yubikey-openpgp-smart-cards-slightly-more-usable/). I simply updated the script to work better with version 4.0 and tweaked it for my setup.
